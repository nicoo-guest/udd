def poolpath(source)
  if source =~ /^lib/
    return source[0..3]+'/'+source
  else
    return source[0..0]+'/'+source
  end
end

def udd_dir
  return '/srv/udd.debian.org/udd/'
end
