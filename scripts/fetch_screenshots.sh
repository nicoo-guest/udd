#!/bin/sh
TARGETDIR=/srv/udd.debian.org/mirrors/screenshots
mkdir -p $TARGETDIR
rm -rf $TARGETDIR/*

# work around a regression in wget from wheezy to jessie
# see https://wiki.debian.org/ServicesSSL#wget
WGET=wget
dir=/etc/ssl/ca-debian
test -d $dir && WGET="wget --ca-directory=$dir"

$WGET -q http://screenshots.debian.net/json/screenshots -O ${TARGETDIR}/screenshots.json
# packages.json just contains less information - but we want it all ...
# wget -q http://screenshots.debian.net/json/packages -O ${TARGETDIR}/packages.json
